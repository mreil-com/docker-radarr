# docker-radarr

[![](https://img.shields.io/bitbucket/pipelines/mreil-com/docker-radarr.svg)](https://bitbucket.org/mreil-com/docker-radarr/addon/pipelines/home)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

> [Radarr](https://github.com/Radarr/Radarr/) image

Based on [mreil/ubuntu-base][ubuntu-base].


## Usage

    docker run -i -t \
    -p 7878:7878 \
    -v ${DATA_DIR}/Radarr/Radarr:/root/.config/Radarr \
    -v ${DATA_DIR}/movies:/root/movies \
    mreil/radarr


## Build

    ./gradlew build

### Update Radarr version

This build always downloads the latest release

### Update Gradle version

    ./gradlew wrapper --gradle-version [VERSION]


## Releases

See [hub.docker.com](https://hub.docker.com/r/mreil/radarr/tags/).


## License

see [LICENSE](LICENSE)


[ubuntu-base]: https://bitbucket.org/mreil-com/docker-ubuntu-base
