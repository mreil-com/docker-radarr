#!/usr/bin/env bash

set -e

# Install dependencies
apt-get update -y
apt-get install -y curl mediainfo libicu74 sqlite3 ca-certificates

cd /tmp

# -O use remote name
# -J use remote header name
# -L location, follow 30x
curl -OJL "http://radarr.servarr.com/v1/update/master/updatefile?os=linux&runtime=netcore&arch=x64"
FILE=$(ls /tmp/Radarr.*.tar.gz)

echo ---------------------------------------
echo Downloaded:
echo $FILE
echo ---------------------------------------

cd /opt
tar xzf $FILE
rm $FILE

# Uninstall unneeded stuff
apt-get remove -y curl
